from companyinfo.models import EmployeesInfo
from django.db.models import Avg, F, Max
from rest_framework.views import APIView
from .serializers import EmployeesInfoSerializer
from rest_framework.response import Response
from rest_framework.mixins import ListModelMixin
from rest_framework.generics import GenericAPIView

# Create your views here.


class HighEEAPI(APIView):
    def get(self, request, format=None):
        dept_info = EmployeesInfo.objects.filter(dept_id__in=[1, 2, 3, 4])
        total_earning_sal = (
            dept_info.annotate(earning=F("sal") + F("comm"))
            .values()
            .filter(dept_id__in=[1, 2, 3, 4])
        )
        dept_info_serializer = EmployeesInfoSerializer(dept_info, many=True)

        dept_maxearn_account = total_earning_sal.filter(
            dept_id=1).aggregate(Max("earning"))
        dept_account_earn = total_earning_sal.filter(
            dept_id=1, earning=dept_maxearn_account["earning__max"])
        dae_serializer = EmployeesInfoSerializer(
            dept_account_earn, many=True)

        dept_maxearn_resarch = total_earning_sal.filter(
            dept_id=2).aggregate(Max("earning"))
        dept_resarch_earn = total_earning_sal.filter(
            dept_id=2, earning=dept_maxearn_resarch["earning__max"])
        dre_serializer = EmployeesInfoSerializer(
            dept_resarch_earn, many=True)

        dept_maxearn_sales = total_earning_sal.filter(
            dept_id=3).aggregate(Max("earning"))
        dept_sales_earn = total_earning_sal.filter(
            dept_id=3, earning=dept_maxearn_sales["earning__max"])
        dse_serializer = EmployeesInfoSerializer(
            dept_sales_earn, many=True)

        dept_maxearn_operation = total_earning_sal.filter(
            dept_id=4).aggregate(Max("earning"))
        dept_operation_earn = total_earning_sal.filter(
            dept_id=4, earning=dept_maxearn_operation["earning__max"])
        doe_serializer = EmployeesInfoSerializer(
            dept_operation_earn, many=True)

        return Response({'dept_info': dept_info_serializer.data,
                         'dept_account_earn': dae_serializer.data,
                         'dept_resarch_earn': dre_serializer.data,
                         'dept_sales_earn': dse_serializer.data,
                         'dept_operation_earn': doe_serializer.data
                         }
                        )


class EmpInfoAPI(APIView):
    def get(self, request, format=None):
        sales_dept = EmployeesInfo.objects.filter(dept_id=3)
        sales_dept_serializer = EmployeesInfoSerializer(sales_dept, many=True)
        emp_info_sale = EmployeesInfo.objects.filter(
            hiredate__lt="1981-11-1", dept_id=3)
        emp_info_sale_serializer = EmployeesInfoSerializer(
            emp_info_sale, many=True)

        return Response({'sales_dept': sales_dept_serializer.data,
                         'emp_info_sale': emp_info_sale_serializer.data})


class SalMIAPI(ListModelMixin, GenericAPIView):
    whole_company_sal_avg = EmployeesInfo.objects.aggregate(Avg("sal"))
    queryset = EmployeesInfo.objects.filter(
        job__in=["SALESMAN", "MANAGER"],
        dept_id__in=[0, 1, 2, 3],
        sal__gt=whole_company_sal_avg["sal__avg"],
    )
    serializer_class = EmployeesInfoSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
