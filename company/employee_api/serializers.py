from rest_framework import serializers
from companyinfo.models import Department, EmployeesInfo


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ['deptno', 'dname', 'loc']


class EmployeesInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeesInfo
        fields = ['empno', 'ename', 'job',
                  'hiredate', 'mgr', 'sal', 'comm', 'dept']
