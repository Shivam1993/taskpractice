"""company URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from companyinfo import views as viewci
from employee_api import views as viewei

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", viewci.HighEarnEmp.as_view(), name='highearnemp'),
    path("empinfo/", viewci.EmpInfo.as_view(), name='empinfo'),
    path("salmanainfo/", viewci.SalManaInfo.as_view(), name='salmaninfo'),
    path("highearnempapi", viewei.HighEEAPI.as_view(), name='highearnempapi'),
    path("empinfoapi/", viewei.EmpInfoAPI.as_view(), name='empinfoapi'),
    path("salmanainfoapi/", viewei.SalMIAPI.as_view(), name='salmaninfoapi'),
]
