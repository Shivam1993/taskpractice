from django.shortcuts import render
from .models import EmployeesInfo
from django.db.models import Avg, F, Max
from django.views import View

# Create your views here.


class HighEarnEmp(View):
    def get(self, request):
        dept_info = EmployeesInfo.objects.filter(dept_id__in=[1, 2, 3, 4])
        total_earning_sal = (
            dept_info.annotate(earning=F("sal") + F("comm"))
            .values()
            .filter(dept_id__in=[1, 2, 3, 4])
        )
        dept_maxearn_account = total_earning_sal.filter(
            dept_id=1).aggregate(Max("earning"))
        dept_account_earn = total_earning_sal.filter(
            dept_id=1, earning=dept_maxearn_account["earning__max"])

        dept_maxearn_resarch = total_earning_sal.filter(
            dept_id=2).aggregate(Max("earning"))
        dept_resarch_earn = total_earning_sal.filter(
            dept_id=2, earning=dept_maxearn_resarch["earning__max"])

        dept_maxearn_sales = total_earning_sal.filter(
            dept_id=3).aggregate(Max("earning"))
        dept_sales_earn = total_earning_sal.filter(
            dept_id=3, earning=dept_maxearn_sales["earning__max"])

        dept_maxearn_operation = total_earning_sal.filter(
            dept_id=4).aggregate(Max("earning"))
        dept_operation_earn = total_earning_sal.filter(
            dept_id=4, earning=dept_maxearn_operation["earning__max"])

        return render(
            request,
            "highearn.html",
            {
                "dept_info": dept_info,
                "total_earning_sal": total_earning_sal,
                "dept_account_earn": dept_account_earn,
                "dept_resarch_earn": dept_resarch_earn,
                "dept_sales_earn": dept_sales_earn,
                "dept_operation_earn": dept_operation_earn,
            },
        )


class EmpInfo(View):
    def get(self, request):
        sales_dept = EmployeesInfo.objects.filter(dept_id=3)
        # emp_info=EmployeesInfo.objects.filter(hiredate__range=('1980-01-01','1981-11-01'),dept_id=3)
        emp_info_sale = EmployeesInfo.objects.filter(
            hiredate__lt="1981-11-1", dept_id=3)

        return render(
            request, "empinfo.html", {
                "sales_dept": sales_dept, "emp_info_sale": emp_info_sale}
        )


class SalManaInfo(View):
    def get(self, request):
        emp_info_all = EmployeesInfo.objects.all()
        whole_company_sal_avg = EmployeesInfo.objects.aggregate(Avg("sal"))
        sales_data = EmployeesInfo.objects.filter(
            job__in=["SALESMAN", "MANAGER"],
            dept_id__in=[0, 1, 2, 3],
            sal__gt=whole_company_sal_avg["sal__avg"],
        )

        return render(
            request,
            "salmanainfo.html",
            {
                "emp_info_all": emp_info_all,
                "sales_data": sales_data,
                "whole_company_sal_avg": whole_company_sal_avg["sal__avg"],
            },
        )
