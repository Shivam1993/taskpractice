from django.db import models

# Create your models here.


class Department(models.Model):
    deptno = models.IntegerField(unique=True, null=False)
    dname = models.CharField(max_length=70)
    loc = models.CharField(max_length=70)

    def __str__(self):
        return str(self.deptno)


class EmployeesInfo(models.Model):
    empno = models.IntegerField(unique=True, null=False)
    ename = models.CharField(max_length=70)
    job = models.CharField(max_length=70)
    hiredate = models.DateField()
    mgr = models.IntegerField(null=True)
    sal = models.IntegerField()
    comm = models.IntegerField(null=True)
    dept = models.ForeignKey(Department, on_delete=models.CASCADE, null=True)
