from django.contrib import admin
from .models import Department, EmployeesInfo

# Register your models here.


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ["deptno", "dname", "loc"]


@admin.register(EmployeesInfo)
class EmployeesInfoAdmin(admin.ModelAdmin):
    list_display = ["empno", "ename", "job",
                    "hiredate", "mgr", "sal", "comm", "dept"]
