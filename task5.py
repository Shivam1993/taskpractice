# create a program to calculate total salary earned by employees in each department.
input_data = {
    "Security": [{"Abhay": "11000"}, {"Sandeep": "13500"}, {"Sarthik": "18500"}],
    "IT": [{"Aman": "18700"}, {"Shikher": "8900"}, {"Saurav": "19760"}],
    "Sales": [{"Vikas": "98200"}, {"Dinesh": "21000"}, {"hardia": "89760"}],
    "HR": [{"Megha": "34000"}, {"Tanisha": "87456"}, {"Karadwal": "7600"}],
    "Devop": [{"Vismay": "45000"}, {"Sumit": "12000"}, {"parthav": "75000"}]
}
def dept_total_sal():
    for dept_info,emp_data in input_data.items():
        total_salary=0
        for index in range(len(emp_data)):
            for total_emp_sal in emp_data[index].values():
                total_salary=total_salary+int(total_emp_sal)
        print(dept_info,total_salary)
print("------------total salary earned by employees in each department--------------")
dept_total_sal()



