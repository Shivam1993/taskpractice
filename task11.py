input_data = {
    "Security": [{"Abhay": "11000"}, {"Sandeep": "13500"}, {"Sarthik": "18500"}],
    "IT": [{"Aman": "18700"}, {"Shikher": "8900"}, {"Saurav": "19760"}],
    "Sales": [{"Vikas": "98200"}, {"Dinesh": "21000"}, {"hardia": "89760"}],
    "HR": [{"Megha": "34000"}, {"Tanisha": "87456"}, {"Karadwal": "7600"}],
    "Devop": [{"Vismay": "45000"}, {"Sumit": "12000"}, {"parthav": "75000"}]
}
def max_dept_amount():
    max_dept ={"dept":"","amount":0}
    for data in input_data:
        total=0
        for emp_info in input_data[data]:
            total+=int([sal for sal in emp_info.values()][0])
            if int(max_dept["amount"])<total:
                max_dept["dept"]=data
                max_dept["amount"]=total
    print(max_dept)
print("-------------Department for Highest Salary--------------")
max_dept_amount()